#Code 7 Responsive Information Map

A responsive module that outputs a google map of a location of your choice and shows optional information relevant to that location that you can customise.