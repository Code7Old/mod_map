<?php

defined('_JEXEC') or die;

?>
	
</style>

<div id="mod_map">
	<h3><?php echo $list['name']; ?></h3>
	
	<input type="checkbox" id="map-toggle">
	<label for="map-toggle" id="map-toggle-label">More Info</label>

	<figure>
		<div>
			<div id="mod_mapMap"></div>
		</div>
		<figcaption>
			<?php

				$i = 1;

				foreach($info as $infoItem){

					echo "<div class='infobox number".$i."'>";
					echo "<p class='infoTitle'>".$infoItem['name']."</p>";
					echo "<p class='inforMation'>".$infoItem['information']."</p>";
					echo "</div>";
					$i++;

				}

			?>
		</figcaption>
	</figure>

</div>

<!-- map options -->
<script>
function initialize() {
	var myOptions = {
       	center: new google.maps.LatLng(<?php echo $list['lat'];?>,<?php echo $list['long'];?>),
       	zoom: <?php echo $list['zoom']; ?>,
       	mapTypeId: google.maps.MapTypeId.ROADMAP,
       	panControl: true,
       	zoomControl: true,
       	zoomControlOptions: {
        style: google.maps.ZoomControlStyle.LARGE
       }       
    };
    
    var map = new google.maps.Map(document.getElementById("mod_mapMap"), myOptions);

    var marker = new google.maps.Marker({
      position: new google.maps.LatLng(<?php echo $list['lat'];?>,<?php echo $list['long'];?>),
      map: map,
      title: 'More info',
      animation: google.maps.Animation.DROP
  	});

  	google.maps.event.addListener(marker, 'click', function() {
  		checkbox = document.getElementById('map-toggle');
  		if(checkbox.checked == false){
    		checkbox.checked=true;
    	} else {
    		checkbox.checked=false;
    	}
  	});
}
</script>